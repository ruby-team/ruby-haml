require 'gem2deb/rake/testtask'

exclude = %w[
  test/haml/filters/coffee_test.rb
  test/haml/filters/markdown_test.rb
]

Gem2Deb::Rake::TestTask.new do |t|
  t.libs = ['test']
  t.test_files = FileList['test/**/*_test.rb'] + FileList['test/**/test_*.rb'] - exclude
  t.ruby_opts = %w[-rtest_helper]
end
